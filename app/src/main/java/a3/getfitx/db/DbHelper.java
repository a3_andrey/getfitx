package a3.getfitx.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DbHelper extends SQLiteOpenHelper {
    static String DB_MEAL_NAME = "meal";
    public static final String TABLE_MEAL_LIST = "meal_list";
    public static final String TABLE_SAVED_MEAL = "saved_list";
    public static final String MEAL_NAME = "meal_name";
    public static final String MEAL_PROTEIN = "meal_protein";
    public static final String MEAL_FAT = "meal_fat";
    public static final String MEAL_CARBOHYDRATE = "meal_carbohydrate";
    public static final String MEAL_CALORIES = "meal_calories";
    public static final String MEAL_WEIGHT = "meal_weight";
    public static final String MEAL_DATE = "meal_date";
    public static final String _ID = "_id";

    public DbHelper(Context context) {
        super(context, DB_MEAL_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_MEAL_LIST + " (" +
                _ID + " INTEGER PRIMARY KEY, " +
                MEAL_NAME + " TEXT, " +
                MEAL_PROTEIN + " TEXT, " +
                MEAL_FAT + " TEXT, " +
                MEAL_CARBOHYDRATE + " TEXT, " +
                MEAL_CALORIES + " TEXT, " +
                MEAL_WEIGHT + " TEXT)";
        String CREATE_TABLE1 = "CREATE TABLE " + TABLE_SAVED_MEAL + " (" +
                _ID + " INTEGER PRIMARY KEY, " +
                MEAL_NAME + " TEXT, " +
                MEAL_PROTEIN + " TEXT, " +
                MEAL_FAT + " TEXT, " +
                MEAL_CARBOHYDRATE + " TEXT, " +
                MEAL_CALORIES + " TEXT, " +
                MEAL_WEIGHT + " TEXT, " +
                MEAL_DATE + " TEXT)";
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE1);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEAL_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVED_MEAL);
        onCreate(db);

    }

}
