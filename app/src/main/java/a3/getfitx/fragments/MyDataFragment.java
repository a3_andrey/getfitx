package a3.getfitx.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import a3.getfitx.R;
import a3.getfitx.filters.FilterDouble;
import a3.getfitx.filters.FilterInt;

public class MyDataFragment extends Fragment {
    private TextView caloriesPerDay, loseWeight, gainWeight, description;
    private RadioButton rbMale, rbFemale;
    private EditText edtWeight, edtHeight, edtAge;
    private Spinner spinnerLifeStyle;
    private String sedentary, lightlyActive, moderatelyActive, veryActive, extraActive;
    private SharedPreferences preferences;
    private double BRM = 0;
    private double brmCoefficient = 0;
    private int genderIndex = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_data_fragment, container, false);

        sedentary = getString(R.string.txt_sedentary);
        lightlyActive = getString(R.string.txt_lightly_active);
        moderatelyActive = getString(R.string.txt_moderately_active);
        veryActive = getString(R.string.txt_very_active);
        extraActive = getString(R.string.txt_extra_active);

        caloriesPerDay = (TextView) view.findViewById(R.id.caloriesPerDay);
        loseWeight = (TextView) view.findViewById(R.id.loseWeight);
        gainWeight = (TextView) view.findViewById(R.id.gainWeight);
        description = (TextView) view.findViewById(R.id.description);

        Button btnCalculateCalPerDay = (Button) view.findViewById(R.id.btnCalculateCalPerDay);

        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        rbMale = (RadioButton) view.findViewById(R.id.rbMale);
        rbFemale = (RadioButton) view.findViewById(R.id.rbFemale);

        edtWeight = (EditText) view.findViewById(R.id.edtWeight);
        edtHeight = (EditText) view.findViewById(R.id.edtHeight);
        edtAge = (EditText) view.findViewById(R.id.edtAge);

        FilterDouble filterDouble = new FilterDouble();
        //setting filters
        edtWeight.setFilters(new InputFilter[]{filterDouble.getFilter()});
        edtHeight.setFilters(new InputFilter[]{filterDouble.getFilter()});
        edtAge.setFilters(new InputFilter[]{ new FilterInt("1", "100")});

        spinnerLifeStyle = (Spinner) view.findViewById(R.id.spActivities);

        //adding lifestyle types to spinnerLifeStyle
        String[] activities = {sedentary, lightlyActive, moderatelyActive, veryActive, extraActive};
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter<CharSequence>(getActivity(), R.layout.spinner_item,
                R.id.simple_spinner_dropdown, activities) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return getView(position, convertView, parent);
            }
        };
        spinnerLifeStyle.setAdapter(arrayAdapter);

        //setting lifestyle
        spinnerLifeStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if(selectedItem.equals(sedentary)) {
                    brmCoefficient = 1.2;
                } else  if (selectedItem.equals(lightlyActive)){
                    brmCoefficient = 1.375;
                } else  if (selectedItem.equals(moderatelyActive)){
                    brmCoefficient = 1.55;
                } else  if (selectedItem.equals(veryActive)){
                    brmCoefficient = 1.725;
                } else  if (selectedItem.equals(extraActive)){
                    brmCoefficient = 1.9;
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        //setting gender
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rbMale:
                        genderIndex = 1;
                        break;
                    case R.id.rbFemale:
                        genderIndex = 2;
                        break;
                }
            }
        });


        btnCalculateCalPerDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edt1 = edtAge.getText().toString();
                String edt2 = edtWeight.getText().toString();
                String edt3 = edtHeight.getText().toString();
                //check fields
                if (edt1.isEmpty()||edt2.isEmpty()||edt3.isEmpty()) {
                    Toast.makeText(getContext(), "Fill in all fields", Toast.LENGTH_LONG).show();
                } else {
                double weight = Double.parseDouble(edtWeight.getText().toString());
                double height = Double.parseDouble(edtHeight.getText().toString());
                double age = Double.parseDouble(edtAge.getText().toString());
                if (genderIndex==1){
                    BRM = (88.36 + (13.4 * weight) + (4.8 * height) - (5.7 * age)) * brmCoefficient;
                } else {
                    BRM = (447.6 + (9.2 * weight) + (3.1 * height) - (4.3 * age)) * brmCoefficient;
                }
                caloriesPerDay.setText(String.valueOf(Math.round(BRM))+ getString(R.string.txt_kcal_day));
                description.setText(R.string.txt_maintain_weight);
                loseWeight.setText(R.string.txt_lose_weight);
                gainWeight.setText(R.string.txt_gain_weight);


                saveInformation();

            }
        }});
        

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //read saved information
        preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        rbMale.setChecked(preferences.getBoolean("selectedMale", false));
        rbFemale.setChecked(preferences.getBoolean("selectedFemale", false));
        edtAge.setText(preferences.getString("age", ""));
        edtWeight.setText(preferences.getString("weight", ""));
        edtHeight.setText(preferences.getString("height", ""));
        spinnerLifeStyle.setSelection((int) preferences.getLong("sp", 0));
        caloriesPerDay.setText(preferences.getString("calories", ""));
        description.setText(preferences.getString("desc", ""));
        loseWeight.setText(preferences.getString("lose", ""));
        gainWeight.setText(preferences.getString("gain", ""));


    }

    //save entered information
    private void saveInformation () {
        boolean selectedMale = rbMale.isChecked();
        boolean selectedFemale = rbFemale.isChecked();
        String age = edtAge.getText().toString();
        String weight = edtWeight.getText().toString();
        String height = edtHeight.getText().toString();
        String calories = caloriesPerDay.getText().toString();
        String desc = description.getText().toString();
        String lose = loseWeight.getText().toString();
        String gain = gainWeight.getText().toString();
        long sp = spinnerLifeStyle.getSelectedItemPosition();

        preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean("selectedMale", selectedMale);
        editor.putBoolean("selectedFemale", selectedFemale);
        editor.putString("age", age);
        editor.putString("weight", weight);
        editor.putString("height", height);
        editor.putLong("sp", sp);
        editor.putString("calories", calories);
        editor.putString("desc", desc);
        editor.putString("lose", lose);
        editor.putString("gain", gain);
        editor.apply();
    }

}

