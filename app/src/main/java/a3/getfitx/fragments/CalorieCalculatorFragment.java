package a3.getfitx.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.activities.NewMealActivity;
import a3.getfitx.adapters.DiaryListAdapter;
import a3.getfitx.db.DbHelper;

public class CalorieCalculatorFragment extends Fragment {

    private DbHelper dbHelper;
    private SQLiteDatabase dataBase;

    private ArrayList<String> mealId = new ArrayList<String>();
    private ArrayList<String> mealName = new ArrayList<String>();
    private ArrayList<String> mealProtein = new ArrayList<String>();
    private ArrayList<String> mealFat = new ArrayList<String>();
    private ArrayList<String> mealCarbohydrate = new ArrayList<String>();
    private ArrayList<String> mealCalories = new ArrayList<String>();
    private ArrayList<String> mealWeight = new ArrayList<String>();
    private ArrayList<String> mealDate = new ArrayList<String>();
    private AlertDialog.Builder build;
    private ListView mealList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.calorie_calculator_fragment, container, false);

        mealList = (ListView) view.findViewById(R.id.lvSavedMeals);
        dbHelper = new DbHelper(getContext());

        Button button = (Button) view.findViewById(R.id.btnAddMeal);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewMealActivity.class);
                startActivity(intent);
            }
        });

        mealList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           final int arg2, long arg3) {

                build = new AlertDialog.Builder(getContext());
                build.setTitle("Delete " + mealName.get(arg2));
                build.setMessage("Do you want to delete ?");
                build.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dataBase.delete(
                                        DbHelper.TABLE_SAVED_MEAL,
                                        DbHelper._ID + "="
                                                + mealId.get(arg2), null);
                                displayData();
                                dialog.cancel();
                            }
                        });

                build.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = build.create();
                alert.show();

                return true;
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        displayData();
        super.onResume();
    }

    private void displayData() {
        dataBase = dbHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DbHelper.TABLE_SAVED_MEAL, null);

        mealId.clear();
        mealName.clear();
        mealProtein.clear();
        mealFat.clear();
        mealCarbohydrate.clear();
        mealCalories.clear();
        mealWeight.clear();
        mealDate.clear();
        if (mCursor.moveToFirst()) {
            do {
                mealId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper._ID)));
                mealName.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_NAME)));
                mealProtein.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_PROTEIN)));
                mealFat.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_FAT)));
                mealCarbohydrate.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_CARBOHYDRATE)));
                mealCalories.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_CALORIES)));
                mealWeight.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_WEIGHT)));
                mealDate.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.MEAL_DATE)));

            } while (mCursor.moveToNext());
        }
        DiaryListAdapter diaryListAdapter = new DiaryListAdapter(getContext(), mealId, mealName,
                mealProtein, mealFat, mealCarbohydrate, mealCalories, mealWeight, mealDate);
        mealList.setAdapter(diaryListAdapter);
        mCursor.close();
    }


}