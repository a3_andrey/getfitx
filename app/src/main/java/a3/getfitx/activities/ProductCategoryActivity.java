package a3.getfitx.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.adapters.ProductListAdapter;
import a3.getfitx.db.ExistingDbHelper;
import a3.getfitx.models.Product;

public class ProductCategoryActivity extends AppCompatActivity {

    private ListView myProductsList;
    private ExistingDbHelper existingDbHelper;
    private SQLiteDatabase dataBase;
    private Cursor productCursor;
    private ArrayList<Product> productList;
    private int tableId;
    private String dbName;
    private ProductListAdapter productListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        //select table for output in list view
        Intent idOfGroup = new Intent(getIntent());
        tableId = idOfGroup.getIntExtra("table_id", 0);

        myProductsList = (ListView) findViewById(R.id.lvProducts);
        switch (tableId) {
            case 1:
                dbName = ExistingDbHelper.TABLE_MY_PRODUCTS;
                toolbar.setTitle(getText(R.string.txt_my_products));
                break;
            case 2:
                dbName = ExistingDbHelper.TABLE_VEGETABLES;
                toolbar.setTitle(getText(R.string.txt_vegetables));
                break;
            case 3:
                dbName = ExistingDbHelper.TABLE_FRUITS_BERRIES;
                toolbar.setTitle(getText(R.string.txt_fruits_berries));
                break;
            case 4:
                dbName = ExistingDbHelper.TABLE_DAIRY_PRODUCTS;
                toolbar.setTitle(getText(R.string.txt_dairy_products));
                break;
            case 5:
                dbName = ExistingDbHelper.TABLE_BREAD_BAKERY;
                toolbar.setTitle(getText(R.string.txt_bread_bakery_products_flour));
                break;
            case 6:
                dbName = ExistingDbHelper.TABLE_MEAT_POULTRY;
                toolbar.setTitle(getText(R.string.txt_meat_poultry));
                break;
            case 7:
                dbName = ExistingDbHelper.TABLE_FISH_SEAFOOD;
                toolbar.setTitle(getText(R.string.txt_fish_seafood));
                break;
            case 8:
                dbName = ExistingDbHelper.TABLE_EGGS;
                toolbar.setTitle(getText(R.string.txt_eggs));
                break;
            case 9:
                dbName = ExistingDbHelper.TABLE_FATS;
                toolbar.setTitle(getText(R.string.txt_fats_margarine_butter));
                break;
            case 10:
                dbName = ExistingDbHelper.TABLE_DRIED_FRUITS;
                toolbar.setTitle(getText(R.string.txt_dried_fruits));
                break;
            case 11:
                dbName = ExistingDbHelper.TABLE_GROATS;
                toolbar.setTitle(getText(R.string.txt_groats));
                break;
            case 12:
                dbName = ExistingDbHelper.TABLE_LEGUMES;
                toolbar.setTitle(getText(R.string.txt_legumes));
                break;
            case 13:
                dbName = ExistingDbHelper.TABLE_MUSHROOMS;
                toolbar.setTitle(getText(R.string.txt_mushrooms));
                break;
            case 14:
                dbName = ExistingDbHelper.TABLE_NUTS_SEEDS;
                toolbar.setTitle(getText(R.string.txt_nuts_seeds));
                break;
            case 15:
                dbName = ExistingDbHelper.TABLE_SAUSAGES;
                toolbar.setTitle(getText(R.string.txt_sausages));
                break;
            case 16:
                dbName = ExistingDbHelper.TABLE_SWEETS;
                toolbar.setTitle(getText(R.string.txt_sweets));
                break;
            case 17:
                dbName = ExistingDbHelper.TABLE_ALCOHOLIC_DRINKS;
                toolbar.setTitle(getText(R.string.txt_alcoholic_drinks));
                break;
            case 18:
                dbName = ExistingDbHelper.TABLE_SOFT_DRINKS;
                toolbar.setTitle(getText(R.string.txt_soft_drinks));
                break;
        }

        //allow adding and editing products if tableId == 1
        if (tableId == 1) {
            myProductsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //get id by click on list view item
                    long productId;
                    if (productCursor.moveToPosition(position)) {
                        productId = Long.parseLong(productCursor.getString(0));
                    } else {
                        throw new NullPointerException();
                    }
                    //sending id to another activity
                    Intent intent = new Intent(ProductCategoryActivity.this, ProductAddEditActivity.class);
                    intent.putExtra("id", productId);
                    startActivity(intent);
                }
            });
        }

        existingDbHelper = new ExistingDbHelper(getApplicationContext());
        existingDbHelper.create_db();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newProduct:
                Intent intent = new Intent(ProductCategoryActivity.this, ProductAddEditActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        productList = new ArrayList<>();
        productList.clear();
        dataBase = existingDbHelper.open();
        productCursor = dataBase.rawQuery("SELECT * FROM " + dbName + " ORDER BY " +
                ExistingDbHelper.COLUMN_NAME + " ASC", null);
        setProduct();
    }

    //product search by name
    public Cursor getProductListByKeyword(String search) {
        productList = new ArrayList<>();
        productList.clear();
        dataBase = existingDbHelper.open();
        productCursor = dataBase.rawQuery("SELECT * FROM " + dbName + " WHERE "
                + ExistingDbHelper.COLUMN_NAME + "  LIKE  '%" + search + "%' ORDER BY " +
                ExistingDbHelper.COLUMN_NAME + " ASC", null);
        setProduct();
        return productCursor;
    }

    private void setProduct() {
        //select  required columns
        if (productCursor != null && productCursor.getCount() != 0) {
            if (productCursor.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setProductId(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_ID)));
                    product.setProductName(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_NAME)));
                    product.setProductProtein(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_PROTEIN)));
                    product.setProductFat(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_FAT)));
                    product.setProductCarbohydrate(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_CARBOHYDRATE)));
                    product.setProductCalories(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_CALORIES)));
                    product.setProductImage(
                            BitmapFactory.decodeByteArray(productCursor.getBlob(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_IMAGE)),
                                    0,
                                    productCursor.getBlob(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_IMAGE)).length));
                    productList.add(product);
                } while (productCursor.moveToNext());
            }
        }
        productListAdapter = new ProductListAdapter(this, productList);
        myProductsList.setAdapter(productListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //the add button is hidden if tableId != 1
        if (tableId != 1) {
            MenuItem menuItem = menu.findItem(R.id.newProduct);
            menuItem.setVisible(false);
        }
        SearchView searchView = (SearchView) menu.findItem(R.id.searchForSee).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                productCursor = getProductListByKeyword(s);
                Toast.makeText(ProductCategoryActivity.this, productCursor.getCount() + " " +
                                getString(R.string.txt_products_found),
                        Toast.LENGTH_LONG)
                        .show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                productCursor = getProductListByKeyword(s);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dataBase.close();
        productCursor.close();
    }
}