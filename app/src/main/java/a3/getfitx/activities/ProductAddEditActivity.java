package a3.getfitx.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

import a3.getfitx.R;
import a3.getfitx.db.ExistingDbHelper;
import a3.getfitx.filters.FilterDouble;

public class ProductAddEditActivity extends AppCompatActivity {

    private EditText edtProductName, edtProductProtein, edtProductFat, edtProductCarbohydrate, edtProductCalories;
    private ImageView productImage;
    private Button btnDelete, btnSave, btnPhoto;
    private Bitmap productPhoto;
    private TextInputLayout inputName, inputProtein, inputFat, inputCarbohydrate, inputCalories;
    private byte[] imageData;
    private static final int CAMERA_REQUEST = 1;
    private SQLiteDatabase dataBase;
    private long productId = 0;
    private ExistingDbHelper existingDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add_edit);

        FilterDouble filterDouble = new FilterDouble();

        edtProductName = (EditText) findViewById(R.id.edtProductName);
        edtProductProtein = (EditText) findViewById(R.id.edtProductProtein);
        edtProductFat = (EditText) findViewById(R.id.edtProductFat);
        edtProductCarbohydrate = (EditText) findViewById(R.id.edtProductCarbohydrate);
        edtProductCalories = (EditText) findViewById(R.id.edtProductCalories);

        edtProductProtein.setFilters(new InputFilter[]{filterDouble.getFilter()});
        edtProductFat.setFilters(new InputFilter[]{filterDouble.getFilter()});
        edtProductCarbohydrate.setFilters(new InputFilter[]{filterDouble.getFilter()});
        edtProductCalories.setFilters(new InputFilter[]{filterDouble.getFilter()});

        productImage = (ImageView) findViewById(R.id.productImage);

        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnPhoto = (Button) findViewById(R.id.btnPhoto);

        inputName = (TextInputLayout) findViewById(R.id.inputName);
        inputProtein = (TextInputLayout) findViewById(R.id.inputProtein);
        inputFat = (TextInputLayout) findViewById(R.id.inputFat);
        inputCarbohydrate = (TextInputLayout) findViewById(R.id.inputCarbohydrate);
        inputCalories = (TextInputLayout) findViewById(R.id.inputCalories);

        existingDbHelper = new ExistingDbHelper(this);
        dataBase = existingDbHelper.open();

        editProduct();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtName = edtProductName.getText().toString();
                String edtProtein = edtProductProtein.getText().toString();
                String edtFat = edtProductFat.getText().toString();
                String edtCarbohydrate = edtProductCarbohydrate.getText().toString();
                String edtCalories = edtProductCalories.getText().toString();

                //check fields
                if (edtName.isEmpty()||edtProtein.isEmpty()||edtFat.isEmpty()||edtCarbohydrate.isEmpty()||edtCalories.isEmpty()) {
                    Toast.makeText(ProductAddEditActivity.this, R.string.fill_in_all_fields, Toast.LENGTH_LONG).show();
                } else {
                    if (productPhoto == null) {
                        productPhoto = BitmapFactory.decodeResource(getResources(),
                                R.drawable.default_product);
                    }
                    //converting photo to byte array
                    ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
                    productPhoto.compress(Bitmap.CompressFormat.PNG, 100, arrayOutputStream);
                    imageData = arrayOutputStream.toByteArray();

                    //getting data from fields and put them to dataBase
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(ExistingDbHelper.COLUMN_NAME, edtProductName.getText().toString());
                    contentValues.put(ExistingDbHelper.COLUMN_PROTEIN, edtProductProtein.getText().toString());
                    contentValues.put(ExistingDbHelper.COLUMN_FAT, edtProductFat.getText().toString());
                    contentValues.put(ExistingDbHelper.COLUMN_CARBOHYDRATE, edtProductCarbohydrate.getText().toString());
                    contentValues.put(ExistingDbHelper.COLUMN_CALORIES, edtProductCalories.getText().toString());
                    contentValues.put(ExistingDbHelper.COLUMN_IMAGE, imageData);
                    //updating dataBase if productId > 0, and inserting new product if productId = 0
                    if (productId > 0) {
                        dataBase.update(ExistingDbHelper.TABLE_MY_PRODUCTS, contentValues,
                                ExistingDbHelper.COLUMN_ID + "=" + String.valueOf(productId), null);
                    } else {
                        dataBase.insert(ExistingDbHelper.TABLE_MY_PRODUCTS, null, contentValues);
                    }
                    goHome();
                }
            }});
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBase.delete(ExistingDbHelper.TABLE_MY_PRODUCTS, ExistingDbHelper.COLUMN_ID + " = ?",
                        new String[]{String.valueOf(productId)});
                goHome();
            }
        });
        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
            }
        });
    }


    private void editProduct() {
        //getting clicked item id
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            productId = extras.getLong("id");
        }
        // getting product data by id from dataBase
        if (productId > 0) {
            Cursor productCursor = dataBase.rawQuery("SELECT * FROM " + ExistingDbHelper.TABLE_MY_PRODUCTS + " where "
                    + ExistingDbHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(productId)});
            if (productCursor != null && productCursor.moveToFirst()) {
                do {
                    edtProductName.setText(productCursor.getString(1));
                    edtProductProtein.setText(productCursor.getString(2));
                    edtProductFat.setText(productCursor.getString(3));
                    edtProductCarbohydrate.setText(productCursor.getString(4));
                    edtProductCalories.setText(productCursor.getString(5));
                    productImage.setImageBitmap(BitmapFactory.
                            decodeByteArray(productCursor.getBlob(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_IMAGE)),
                                    0,
                                    productCursor.getBlob(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_IMAGE)).length));
                    productCursor.close();
                } while (productCursor.moveToNext());
            }
        } else {
            //hide the delete button
            btnDelete.setVisibility(View.GONE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //getting product photo from device camera
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            productPhoto = (Bitmap) data.getExtras().get("data");
            productImage.setImageBitmap(productPhoto);
        } else {
            productPhoto = BitmapFactory.decodeResource(getResources(),
                    R.drawable.default_product);
        }
    }

    private void goHome() {
        dataBase.close();
        Intent intent = new Intent(this, ProductCategoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}