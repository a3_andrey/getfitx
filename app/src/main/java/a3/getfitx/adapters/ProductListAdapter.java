package a3.getfitx.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.models.Product;

public class ProductListAdapter extends BaseAdapter {

    Context context;
    ArrayList<Product> productList;

    public ProductListAdapter(Context context, ArrayList<Product> employeeList) {
        this.context = context;
        this.productList = employeeList;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int position) {
        return productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imgProduct;
        TextView txtProductName;
        TextView txtProductProtein;
        TextView txtProductFat;
        TextView txtProductCarbohydrate;
        TextView txtProductCalories;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.product_list_row, null);

            holder = new ViewHolder();
            holder.txtProductName = (TextView) convertView.findViewById(R.id.txtProductName);
            holder.txtProductProtein = (TextView) convertView.findViewById(R.id.txtProductProtein);
            holder.txtProductFat = (TextView) convertView.findViewById(R.id.txtProductFat);
            holder.txtProductCarbohydrate = (TextView) convertView.findViewById(R.id.txtProductCarbohydrate);
            holder.txtProductCalories = (TextView) convertView.findViewById(R.id.txtProductCalories);
            holder.imgProduct = (ImageView) convertView.findViewById(R.id.imgProduct);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Product product = productList.get(position);
        holder.txtProductName.setText(product.getProductName());
        holder.txtProductProtein.setText(product.getProductProtein());
        holder.txtProductFat.setText(product.getProductFat());
        holder.txtProductCarbohydrate.setText(product.getProductCarbohydrate());
        holder.txtProductCalories.setText(product.getProductCalories());
        holder.imgProduct.setImageBitmap(product.getProductImage());

        return convertView;
    }
}
