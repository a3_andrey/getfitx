package a3.getfitx.activities;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.adapters.ProductListAdapter;
import a3.getfitx.db.ExistingDbHelper;
import a3.getfitx.db.DbHelper;
import a3.getfitx.filters.FilterInt;
import a3.getfitx.models.Product;

public class NewProductSearchActivity extends AppCompatActivity {
    private ListView searchedList;
    private ExistingDbHelper existingDbHelper;
    private Cursor productCursor;
    private ArrayList<Product> productList;
    private String strMyProducts, strVegetables, strFruits, strDairy, strBread, strMeat, strFish,
            strEggs, strFats, strDried, strGroats, strLegumes, strMushrooms, strNuts, strSausages,
            strSweets, strAlcohol, strSoft, tableName;
    private ArrayAdapter<String> mAdapter;
    private EditText setProductName, setProductWeight, setProductProtein, setProductFat,
            setProductCarbohydrate, setProductCalories;
    private DbHelper dbHelper;
    private SQLiteDatabase dataBase;
    private AlertDialog.Builder dialogBuilder;
    private LayoutInflater inflater;
    private View dialogView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product_search);

        searchedList = (ListView) findViewById(R.id.lvSearchedProducts);

        Spinner spinnerCategories = (Spinner) findViewById(R.id.spCategories);

        strMyProducts = (String) getText(R.string.txt_my_products);
        strVegetables = (String) getText(R.string.txt_vegetables);
        strFruits = (String) getText(R.string.txt_fruits_berries);
        strDairy = (String) getText(R.string.txt_dairy_products);
        strBread = (String) getText(R.string.txt_bread_bakery_products_flour);
        strMeat = (String) getText(R.string.txt_meat_poultry);
        strFish = (String) getText(R.string.txt_fish_seafood);
        strEggs = (String) getText(R.string.txt_eggs);
        strFats = (String) getText(R.string.txt_fats_margarine_butter);
        strDried = (String) getText(R.string.txt_dried_fruits);
        strGroats = (String) getText(R.string.txt_groats);
        strLegumes = (String) getText(R.string.txt_legumes);
        strMushrooms = (String) getText(R.string.txt_mushrooms);
        strNuts = (String) getText(R.string.txt_nuts_seeds);
        strSausages = (String) getText(R.string.txt_sausages);
        strSweets = (String) getText(R.string.txt_sweets);
        strAlcohol = (String) getText(R.string.txt_alcoholic_drinks);
        strSoft = (String) getText(R.string.txt_soft_drinks);

        setProductWeight = (EditText) findViewById(R.id.setProductWeight);
        setProductProtein = (EditText) findViewById(R.id.setProductProtein);
        setProductFat = (EditText) findViewById(R.id.setProductFat);
        setProductCarbohydrate = (EditText) findViewById(R.id.setProductCarbohydrate);
        setProductCalories = (EditText) findViewById(R.id.setProductCalories);

        dbHelper = new DbHelper(this);

        tableName = ExistingDbHelper.TABLE_MY_PRODUCTS;
        // spinner items names assignment
        String[] activities = {strMyProducts, strVegetables, strFruits, strDairy, strBread, strMeat,
                strFish, strEggs, strFats, strDried, strGroats, strLegumes, strMushrooms, strNuts,
                strSausages, strSweets, strAlcohol, strSoft};
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.spinner_item,
                R.id.simple_spinner_dropdown, activities) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return getView(position, convertView, parent);
            }
        };
        spinnerCategories.setAdapter(arrayAdapter);


        //change table when choosing spinner item
        spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (selectedItem.equals(strMyProducts)) {
                    tableName = ExistingDbHelper.TABLE_MY_PRODUCTS;
                } else if (selectedItem.equals(strVegetables)) {
                    tableName = ExistingDbHelper.TABLE_VEGETABLES;
                } else if (selectedItem.equals(strFruits)) {
                    tableName = ExistingDbHelper.TABLE_FRUITS_BERRIES;
                } else if (selectedItem.equals(strDairy)) {
                    tableName = ExistingDbHelper.TABLE_DAIRY_PRODUCTS;
                } else if (selectedItem.equals(strBread)) {
                    tableName = ExistingDbHelper.TABLE_BREAD_BAKERY;
                } else if (selectedItem.equals(strMeat)) {
                    tableName = ExistingDbHelper.TABLE_MEAT_POULTRY;
                } else if (selectedItem.equals(strFish)) {
                    tableName = ExistingDbHelper.TABLE_FISH_SEAFOOD;
                } else if (selectedItem.equals(strEggs)) {
                    tableName = ExistingDbHelper.TABLE_EGGS;
                } else if (selectedItem.equals(strFats)) {
                    tableName = ExistingDbHelper.TABLE_FATS;
                } else if (selectedItem.equals(strDried)) {
                    tableName = ExistingDbHelper.TABLE_DRIED_FRUITS;
                } else if (selectedItem.equals(strGroats)) {
                    tableName = ExistingDbHelper.TABLE_GROATS;
                } else if (selectedItem.equals(strLegumes)) {
                    tableName = ExistingDbHelper.TABLE_LEGUMES;
                } else if (selectedItem.equals(strMushrooms)) {
                    tableName = ExistingDbHelper.TABLE_MUSHROOMS;
                } else if (selectedItem.equals(strNuts)) {
                    tableName = ExistingDbHelper.TABLE_NUTS_SEEDS;
                } else if (selectedItem.equals(strSausages)) {
                    tableName = ExistingDbHelper.TABLE_SAUSAGES;
                } else if (selectedItem.equals(strSweets)) {
                    tableName = ExistingDbHelper.TABLE_SWEETS;
                } else if (selectedItem.equals(strAlcohol)) {
                    tableName = ExistingDbHelper.TABLE_ALCOHOLIC_DRINKS;
                } else if (selectedItem.equals(strSoft)) {
                    tableName = ExistingDbHelper.TABLE_SOFT_DRINKS;
                }
                update();
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //get id by click on list view item
                long productId;
                if (productCursor.moveToPosition(position)) {
                    productId = Long.parseLong(productCursor.getString(0));
                } else {
                    throw new NullPointerException();
                }

                dialogBuilder = new AlertDialog.Builder(NewProductSearchActivity.this);
                inflater = NewProductSearchActivity.this.getLayoutInflater();
                dialogView = inflater.inflate(R.layout.alert_dialog_new_meal, null);
                dialogBuilder.setView(dialogView);

                setProductName = (EditText) dialogView.findViewById(R.id.setProductName);
                setProductWeight = (EditText) dialogView.findViewById(R.id.setProductWeight);
                setProductProtein = (EditText) dialogView.findViewById(R.id.setProductProtein);
                setProductFat = (EditText) dialogView.findViewById(R.id.setProductFat);
                setProductCarbohydrate = (EditText) dialogView.findViewById(R.id.setProductCarbohydrate);
                setProductCalories = (EditText) dialogView.findViewById(R.id.setProductCalories);
                setProductWeight.setFilters(new InputFilter[]{new FilterInt("1", "1000")});

                Cursor productCursor = dataBase.rawQuery("SELECT * FROM " + tableName + " WHERE "
                        + ExistingDbHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(productId)});
                if (productCursor != null && productCursor.moveToFirst()) {
                    do {
                        final String name = productCursor.getString(1);
                        final double protein = Double.parseDouble(productCursor.getString(2));
                        final double fat = Double.parseDouble(productCursor.getString(3));
                        final double carbohydrate = Double.parseDouble(productCursor.getString(4));
                        final double calories = Double.parseDouble(productCursor.getString(5));
                        setProductName.setText(name);

                        //change of characteristics depending on weight
                        setProductWeight.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (!setProductWeight.getText().toString().equalsIgnoreCase("")) {
                                    double d = Double.parseDouble((setProductWeight.getText().toString()));
                                    setProductProtein.setText(String.valueOf(Math.floor(((d * protein) / 100) * 100) / 100));
                                    setProductFat.setText(String.valueOf(Math.floor(((d * fat) / 100) * 100) / 100));
                                    setProductCarbohydrate.setText(String.valueOf(Math.floor(((d * carbohydrate) / 100) * 100) / 100));
                                    setProductCalories.setText(String.valueOf(Math.floor(((d * calories) / 100) * 100) / 100));
                                }
                            }
                            @Override
                            public void afterTextChanged(Editable s) {
                            }
                        });

                        productCursor.close();
                    } while (productCursor.moveToNext());
                }

                dialogBuilder.setTitle(R.string.Product_selection);
                dialogBuilder.setMessage(R.string.enter_weight_below);
                dialogBuilder.setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dataBase = dbHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(DbHelper.MEAL_NAME, setProductName.getText().toString());
                        values.put(DbHelper.MEAL_PROTEIN, setProductProtein.getText().toString());
                        values.put(DbHelper.MEAL_FAT, setProductFat.getText().toString());
                        values.put(DbHelper.MEAL_CARBOHYDRATE, setProductCarbohydrate.getText().toString());
                        values.put(DbHelper.MEAL_CALORIES, setProductCalories.getText().toString());
                        values.put(DbHelper.MEAL_WEIGHT, setProductWeight.getText().toString());
                        //insert data into database
                        dataBase.insert(DbHelper.TABLE_MEAL_LIST, null, values);
                        //close database
                        dataBase.close();
                        Intent intent = new Intent(NewProductSearchActivity.this, NewMealActivity.class);
                        startActivity(intent);
                    }
                });
                dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });

        SearchView searchView = (SearchView) findViewById(R.id.searchForCalculate);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                productCursor = getProductListByKeyword(s);
                Toast.makeText(NewProductSearchActivity.this, productCursor.getCount() + " " +
                                getString(R.string.txt_products_found),
                        Toast.LENGTH_LONG)
                        .show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                productCursor = getProductListByKeyword(s);
                return false;
            }
        });
        existingDbHelper = new ExistingDbHelper(getApplicationContext());
        //creating db
        existingDbHelper.create_db();
    }

    private void update() {
        productList = new ArrayList<>();
        productList.clear();
        dataBase = existingDbHelper.open();
        productCursor = dataBase.rawQuery("SELECT * FROM " + tableName + " ORDER BY "
                + ExistingDbHelper.COLUMN_NAME + " ASC", null);
        setProduct();
    }


    //product search by name
    public Cursor getProductListByKeyword(String search) {
        productList = new ArrayList<>();
        productList.clear();
        dataBase = existingDbHelper.open();
        //get data from db by word
        String sql1 = "SELECT * FROM " + tableName + " WHERE " + ExistingDbHelper.COLUMN_NAME + "  LIKE  '%"
                + search + "%'  ORDER BY " + ExistingDbHelper.COLUMN_NAME + " ASC";
        productCursor = dataBase.rawQuery(sql1, null);
        setProduct();
        return productCursor;
    }

    private void setProduct() {
        //select  required columns
        if (productCursor != null && productCursor.getCount() != 0) {
            if (productCursor.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setProductName(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_NAME)));
                    product.setProductProtein(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_PROTEIN)));
                    product.setProductFat(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_FAT)));
                    product.setProductCarbohydrate(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_CARBOHYDRATE)));
                    product.setProductCalories(productCursor.getString(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_CALORIES)));
                    product.setProductImage(
                            BitmapFactory.decodeByteArray(productCursor.getBlob(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_IMAGE)),
                                    0,
                                    productCursor.getBlob(productCursor.getColumnIndex(ExistingDbHelper.COLUMN_IMAGE)).length));
                    productList.add(product);
                } while (productCursor.moveToNext());
            }
        }
        ProductListAdapter productListAdapter = new ProductListAdapter(this, productList);
        searchedList.setAdapter(productListAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dataBase.close();
        productCursor.close();
    }
}
