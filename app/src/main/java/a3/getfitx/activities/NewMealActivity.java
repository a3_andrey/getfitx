package a3.getfitx.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.adapters.MealListAdapter;
import a3.getfitx.db.DbHelper;

public class NewMealActivity extends Activity {

    private DbHelper dbHelper;
    private SQLiteDatabase dataBase;
    private ArrayList<String> mealId = new ArrayList<String>();
    private ArrayList<String> mealName = new ArrayList<String>();
    private ArrayList<String> mealProtein = new ArrayList<String>();
    private ArrayList<String> mealFat = new ArrayList<String>();
    private ArrayList<String> mealCarbohydrate = new ArrayList<String>();
    private ArrayList<String> mealCalories = new ArrayList<String>();
    private ArrayList<String> mealWeight = new ArrayList<String>();
    private ListView mealList;
    private AlertDialog.Builder dialogBuilder;
    private LayoutInflater inflater;
    private View dialogView;
    private TextView txtTotalWeight, txtTotalCalories, txtTotalProtein, txtTotalFat, txtTotalCarbohydrate;
    private EditText setMealName, setMealWeight, setMealProtein, setMealFat, setMealCarbohydrate, setMealCalories, setMealDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_meal);

        mealList = (ListView) findViewById(R.id.lvNewSet);
        dbHelper = new DbHelper(this);

        txtTotalWeight = (TextView) findViewById(R.id.txtTotalWeight);
        txtTotalCalories = (TextView) findViewById(R.id.txtTotalCalories);
        txtTotalProtein = (TextView) findViewById(R.id.txtTotalProtein);
        txtTotalFat = (TextView) findViewById(R.id.txtTotalFat);
        txtTotalCarbohydrate = (TextView) findViewById(R.id.txtTotalCarbohydrate);

        Button btnAddProduct = (Button) findViewById(R.id.btnAddProduct);
        btnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewMealActivity.this, NewProductSearchActivity.class);
                startActivity(intent);
            }
        });

        //saving data in a diary
        Button btnSaveMeal = (Button) findViewById(R.id.btnSaveMeal);
        btnSaveMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBase = dbHelper.getWritableDatabase();

                dialogBuilder = new AlertDialog.Builder(NewMealActivity.this);
                inflater = NewMealActivity.this.getLayoutInflater();
                dialogView = inflater.inflate(R.layout.alert_dialog_new_diary, null);
                dialogBuilder.setView(dialogView);

                setMealName = (EditText) dialogView.findViewById(R.id.setMealName);
                setMealWeight = (EditText) dialogView.findViewById(R.id.setMealWeight);
                setMealProtein = (EditText) dialogView.findViewById(R.id.setMealProtein);
                setMealFat = (EditText) dialogView.findViewById(R.id.setMealFat);
                setMealCarbohydrate = (EditText) dialogView.findViewById(R.id.setMealCarbohydrate);
                setMealCalories = (EditText) dialogView.findViewById(R.id.setMealCalories);
                setMealDate = (EditText) dialogView.findViewById(R.id.setMealDate);

                setMealWeight.setText(txtTotalWeight.getText().toString());
                setMealProtein.setText(txtTotalProtein.getText().toString());
                setMealFat.setText(txtTotalFat.getText().toString());
                setMealCarbohydrate.setText(txtTotalCarbohydrate.getText().toString());
                setMealCalories.setText(txtTotalCalories.getText().toString());

                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy, h:mm a");
                String dateString = sdf.format(System.currentTimeMillis());
                setMealDate.setText(dateString);

                dialogBuilder.setTitle(R.string.new_meal_creation);
                dialogBuilder.setMessage(R.string.enter_name_below);
                dialogBuilder.setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ContentValues values = new ContentValues();
                        values.put(DbHelper.MEAL_NAME, setMealName.getText().toString());
                        values.put(DbHelper.MEAL_PROTEIN, setMealProtein.getText().toString());
                        values.put(DbHelper.MEAL_FAT, setMealFat.getText().toString());
                        values.put(DbHelper.MEAL_CARBOHYDRATE, setMealCarbohydrate.getText().toString());
                        values.put(DbHelper.MEAL_CALORIES, setMealCalories.getText().toString());
                        values.put(DbHelper.MEAL_WEIGHT, setMealWeight.getText().toString());
                        values.put(DbHelper.MEAL_DATE, setMealDate.getText().toString());
                        //insert data into database
                        dataBase.insert(DbHelper.TABLE_SAVED_MEAL, null, values);
                        //close database
                        dataBase.delete(DbHelper.TABLE_MEAL_LIST, null, null);
                        dataBase.close();

                        Intent intent = new Intent(NewMealActivity.this, MainActivity.class);
                        intent.putExtra("id", 1);
                        startActivity(intent);
                    }

                });
                dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });
    }


    public void deleteTask(View view) {
        View parent = (View) view.getParent();
        TextView productName = (TextView) parent.findViewById(R.id.txtProductName);
        String name = String.valueOf(productName.getText());
        dataBase = dbHelper.getWritableDatabase();
        dataBase.delete(DbHelper.TABLE_MEAL_LIST, DbHelper.MEAL_NAME + " = ?", new String[]{name});
        dataBase.close();
        displayData();
        calculate();
    }

    @Override
    protected void onResume() {
        displayData();
        calculate();
        super.onResume();
    }

    //calculation of the sum of all indicators
    private void calculate() {
        Cursor cWeight = dataBase.rawQuery("SELECT SUM ( " + DbHelper.MEAL_WEIGHT + " ) FROM " + DbHelper.TABLE_MEAL_LIST, null);
        if (cWeight.moveToFirst()) {
            txtTotalWeight.setText(Integer.toString(cWeight.getInt(0)));
        }

        Cursor cCalories = dataBase.rawQuery("SELECT SUM ( " + DbHelper.MEAL_CALORIES + " ) FROM " + DbHelper.TABLE_MEAL_LIST, null);
        if (cCalories.moveToFirst()) {
            txtTotalCalories.setText(Integer.toString(cCalories.getInt(0)));
        }

        Cursor cProtein = dataBase.rawQuery("SELECT SUM ( " + DbHelper.MEAL_PROTEIN + " ) FROM " + DbHelper.TABLE_MEAL_LIST, null);
        if (cProtein.moveToFirst()) {
            txtTotalProtein.setText(Double.toString(cProtein.getDouble(0)));
        }

        Cursor cFat = dataBase.rawQuery("SELECT SUM ( " + DbHelper.MEAL_FAT + " ) FROM " + DbHelper.TABLE_MEAL_LIST, null);
        if (cFat.moveToFirst()) {
            txtTotalFat.setText(Double.toString(cFat.getDouble(0)));
        }

        Cursor cCarbohydrate = dataBase.rawQuery("SELECT SUM ( " + DbHelper.MEAL_CARBOHYDRATE + " ) FROM " + DbHelper.TABLE_MEAL_LIST, null);
        if (cCarbohydrate.moveToFirst()) {
            txtTotalCarbohydrate.setText(Double.toString(cCarbohydrate.getDouble(0)));
        }
    }

    private void displayData() {
        dataBase = dbHelper.getWritableDatabase();
        Cursor mealCursor = dataBase.rawQuery("SELECT * FROM " + DbHelper.TABLE_MEAL_LIST, null);
        mealId.clear();
        mealName.clear();
        mealProtein.clear();
        mealFat.clear();
        mealCarbohydrate.clear();
        mealCalories.clear();
        mealWeight.clear();
        if (mealCursor.moveToFirst()) {
            do {
                mealId.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper._ID)));
                mealName.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper.MEAL_NAME)));
                mealProtein.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper.MEAL_PROTEIN)));
                mealFat.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper.MEAL_FAT)));
                mealCarbohydrate.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper.MEAL_CARBOHYDRATE)));
                mealCalories.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper.MEAL_CALORIES)));
                mealWeight.add(mealCursor.getString(mealCursor.getColumnIndex(DbHelper.MEAL_WEIGHT)));

            } while (mealCursor.moveToNext());
        }
        MealListAdapter mealListAdapter = new MealListAdapter(NewMealActivity.this, mealId, mealName, mealProtein, mealFat, mealCarbohydrate, mealCalories, mealWeight);
        mealList.setAdapter(mealListAdapter);
        mealCursor.close();
    }

}