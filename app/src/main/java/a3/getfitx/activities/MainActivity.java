package a3.getfitx.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import a3.getfitx.R;
import a3.getfitx.adapters.ViewPagerAdapter;

public class MainActivity extends FragmentActivity {

    private String txtFragmentMyData;
    private String txtFragmentProductList;
    private String txtFragmentCalorieCalculator;
    private TabLayout tabLayout;
    private int tabId;
    private ViewPager viewPager;
    private ViewPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtFragmentMyData = getString(R.string.txt_fragment_my_data);
        txtFragmentProductList = getString(R.string.txt_fragment_product_list);
        txtFragmentCalorieCalculator = getString(R.string.txt_fragment_calorie_calculator);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        //tab names assignment
        tabLayout.addTab(tabLayout.newTab().setText(txtFragmentMyData));
        tabLayout.addTab(tabLayout.newTab().setText(txtFragmentProductList));
        tabLayout.addTab(tabLayout.newTab().setText(txtFragmentCalorieCalculator));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tabId = extras.getInt("id", 1);
        }
        if (tabId > 0) {
            viewPager.setCurrentItem(2);
        } else {
            viewPager.setCurrentItem(1);
        }
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}

