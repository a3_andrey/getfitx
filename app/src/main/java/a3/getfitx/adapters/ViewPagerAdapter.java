package a3.getfitx.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import a3.getfitx.fragments.CalorieCalculatorFragment;
import a3.getfitx.fragments.MyDataFragment;
import a3.getfitx.fragments.ProductListFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private int numOfTabs;

    public ViewPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MyDataFragment tabMyData = new MyDataFragment();
                return tabMyData;
            case 1:
                ProductListFragment tabProductList = new ProductListFragment();
                return tabProductList;
            case 2:
                CalorieCalculatorFragment tabCalorieCalculator = new CalorieCalculatorFragment();
                return tabCalorieCalculator;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}