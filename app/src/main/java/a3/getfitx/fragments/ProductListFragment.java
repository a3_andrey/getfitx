package a3.getfitx.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.adapters.CardViewAdapter;


public class ProductListFragment extends Fragment {

    private ArrayList<String> productGroups;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_list_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rvCards);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        //setting names to card views
        productGroups = new ArrayList<>();
        productGroups.add(getString(R.string.txt_my_products));
        productGroups.add(getString(R.string.txt_vegetables));
        productGroups.add(getString(R.string.txt_fruits_berries));
        productGroups.add(getString(R.string.txt_dairy_products));
        productGroups.add(getString(R.string.txt_bread_bakery_products_flour));
        productGroups.add(getString(R.string.txt_meat_poultry));
        productGroups.add(getString(R.string.txt_fish_seafood));
        productGroups.add(getString(R.string.txt_eggs));
        productGroups.add(getString(R.string.txt_fats_margarine_butter));
        productGroups.add(getString(R.string.txt_dried_fruits));
        productGroups.add(getString(R.string.txt_groats));
        productGroups.add(getString(R.string.txt_legumes));
        productGroups.add(getString(R.string.txt_mushrooms));
        productGroups.add(getString(R.string.txt_nuts_seeds));
        productGroups.add(getString(R.string.txt_sausages));
        productGroups.add(getString(R.string.txt_sweets));
        productGroups.add(getString(R.string.txt_alcoholic_drinks));
        productGroups.add(getString(R.string.txt_soft_drinks));

        RecyclerView.Adapter<CardViewAdapter.ViewHolder> adapter = new CardViewAdapter(productGroups);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(getActivity().getApplicationContext(),
                    new GestureDetector.SimpleOnGestureListener() {

                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }

                    });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && gestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(child);

                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        return view;
    }


}