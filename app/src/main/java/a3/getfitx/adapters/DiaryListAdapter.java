package a3.getfitx.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import a3.getfitx.R;


public class DiaryListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<String> id;
    private ArrayList<String> name;
    private ArrayList<String> protein;
    private ArrayList<String> fat;
    private ArrayList<String> carbohydrate;
    private ArrayList<String> calories;
    private ArrayList<String> weight;
    private ArrayList<String> date;

    public DiaryListAdapter(Context mContext, ArrayList<String> id, ArrayList<String> mealName,
                            ArrayList<String> mealProtein, ArrayList<String> mealFat,
                            ArrayList<String> mealCarbohydrate, ArrayList<String> mealCalories,
                            ArrayList<String> mealWeight, ArrayList<String> mealDate) {
        this.mContext = mContext;
        this.id = id;
        this.name = mealName;
        this.protein = mealProtein;
        this.fat = mealFat;
        this.carbohydrate = mealCarbohydrate;
        this.calories = mealCalories;
        this.weight = mealWeight;
        this.date = mealDate;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int pos, View child, ViewGroup parent) {
        Holder mHolder;
        LayoutInflater layoutInflater;
        if (child == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.diary_list_row, null);

            mHolder = new Holder();
            mHolder.txtMealName = (TextView) child.findViewById(R.id.txtMealName);
            mHolder.txtMealProtein = (TextView) child.findViewById(R.id.txtMealProtein);
            mHolder.txtMealFat = (TextView) child.findViewById(R.id.txtMealFat);
            mHolder.txtMealCarbohydrate = (TextView) child.findViewById(R.id.txtMealCarbohydrate);
            mHolder.txtMealCalories = (TextView) child.findViewById(R.id.txtMealCalories);
            mHolder.txtMealWeight = (TextView) child.findViewById(R.id.txtMealWeight);
            mHolder.txtMealDate = (TextView) child.findViewById(R.id.txtMealDate);
            child.setTag(mHolder);
        } else {
            mHolder = (Holder) child.getTag();
        }

        mHolder.txtMealName.setText(name.get(pos));
        mHolder.txtMealProtein.setText(protein.get(pos));
        mHolder.txtMealFat.setText(fat.get(pos));
        mHolder.txtMealCarbohydrate.setText(carbohydrate.get(pos));
        mHolder.txtMealCalories.setText(calories.get(pos));
        mHolder.txtMealWeight.setText(weight.get(pos));
        mHolder.txtMealDate.setText(date.get(pos));

        return child;
    }

    public class Holder {
        TextView txtMealName;
        TextView txtMealProtein;
        TextView txtMealFat;
        TextView txtMealCarbohydrate;
        TextView txtMealCalories;
        TextView txtMealWeight;
        TextView txtMealDate;
    }
}
