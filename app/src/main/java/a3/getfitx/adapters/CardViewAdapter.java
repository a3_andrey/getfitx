package a3.getfitx.adapters;


import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import a3.getfitx.R;
import a3.getfitx.activities.ProductCategoryActivity;

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {
    private ArrayList<String> productGroups;

    public CardViewAdapter(ArrayList<String> productGroups) {
        this.productGroups = productGroups;
    }

    @Override
    public CardViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_product_category, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardViewAdapter.ViewHolder viewHolder, int i) {
        viewHolder.txtGroupName.setText(productGroups.get(i));
    }

    @Override
    public int getItemCount() {
        return productGroups.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtGroupName;

        public ViewHolder(final View view) {
            super(view);

            txtGroupName = (TextView) view.findViewById(R.id.txtNameOfGroup);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(view.getContext(), ProductCategoryActivity.class);
                    switch (getLayoutPosition()) {
                        case 0:
                            intent.putExtra("table_id", 1);
                            view.getContext().startActivity(intent);
                            break;
                        case 1:
                            intent.putExtra("table_id", 2);
                            view.getContext().startActivity(intent);
                            break;
                        case 2:
                            intent.putExtra("table_id", 3);
                            view.getContext().startActivity(intent);
                            break;
                        case 3:
                            intent.putExtra("table_id", 4);
                            view.getContext().startActivity(intent);
                            break;
                        case 4:
                            intent.putExtra("table_id", 5);
                            view.getContext().startActivity(intent);
                            break;
                        case 5:
                            intent.putExtra("table_id", 6);
                            view.getContext().startActivity(intent);
                            break;
                        case 6:
                            intent.putExtra("table_id", 7);
                            view.getContext().startActivity(intent);
                            break;
                        case 7:
                            intent.putExtra("table_id", 8);
                            view.getContext().startActivity(intent);
                            break;
                        case 8:
                            intent.putExtra("table_id", 9);
                            view.getContext().startActivity(intent);
                            break;
                        case 9:
                            intent.putExtra("table_id", 10);
                            view.getContext().startActivity(intent);
                            break;
                        case 10:
                            intent.putExtra("table_id", 11);
                            view.getContext().startActivity(intent);
                            break;
                        case 11:
                            intent.putExtra("table_id", 12);
                            view.getContext().startActivity(intent);
                            break;
                        case 12:
                            intent.putExtra("table_id", 13);
                            view.getContext().startActivity(intent);
                            break;
                        case 13:
                            intent.putExtra("table_id", 14);
                            view.getContext().startActivity(intent);
                            break;
                        case 14:
                            intent.putExtra("table_id", 15);
                            view.getContext().startActivity(intent);
                            break;
                        case 15:
                            intent.putExtra("table_id", 16);
                            view.getContext().startActivity(intent);
                            break;
                        case 16:
                            intent.putExtra("table_id", 17);
                            view.getContext().startActivity(intent);
                            break;
                        case 17:
                            intent.putExtra("table_id", 18);
                            view.getContext().startActivity(intent);
                            break;
                    }

                }
            });
        }
    }
}