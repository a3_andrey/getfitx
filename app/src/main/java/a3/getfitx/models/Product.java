package a3.getfitx.models;


import android.graphics.Bitmap;

public class Product {
    private String productId;
    private String productName;
    private String productProtein;
    private String productFat;
    private String productCarbohydrate;
    private String productCalories;
    private Bitmap productImage;


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductProtein() {
        return productProtein;
    }

    public void setProductProtein(String productProtein) {
        this.productProtein = productProtein;
    }

    public String getProductFat() {
        return productFat;
    }

    public void setProductFat(String productFat) {
        this.productFat = productFat;
    }

    public String getProductCarbohydrate() {
        return productCarbohydrate;
    }

    public void setProductCarbohydrate(String productCarbohydrate) {
        this.productCarbohydrate = productCarbohydrate;
    }

    public String getProductCalories() {
        return productCalories;
    }

    public void setProductCalories(String productCalories) {
        this.productCalories = productCalories;
    }


    public Bitmap getProductImage() {
        return productImage;
    }

    public void setProductImage(Bitmap productImage) {
        this.productImage = productImage;
    }
}
