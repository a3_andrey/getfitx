package a3.getfitx.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ExistingDbHelper extends SQLiteOpenHelper {
    private static String DB_PATH;
    private static String DB_NAME = "Products.db";
    private static final int SCHEMA = 1;

    public static final String TABLE_MY_PRODUCTS = "my_products";
    public static final String TABLE_VEGETABLES = "vegetables";
    public static final String TABLE_FRUITS_BERRIES = "fruits_berries";
    public static final String TABLE_DAIRY_PRODUCTS = "dairy_products";
    public static final String TABLE_BREAD_BAKERY = "bread_bakery";
    public static final String TABLE_MEAT_POULTRY = "meat_poultry";
    public static final String TABLE_FISH_SEAFOOD = "fish_seafood";
    public static final String TABLE_EGGS = "eggs";
    public static final String TABLE_FATS = "fats";
    public static final String TABLE_DRIED_FRUITS = "dried_fruits";
    public static final String TABLE_GROATS = "groats";
    public static final String TABLE_LEGUMES = "legumes";
    public static final String TABLE_MUSHROOMS = "mushrooms";
    public static final String TABLE_NUTS_SEEDS = "nuts_seeds";
    public static final String TABLE_SAUSAGES = "sausages";
    public static final String TABLE_SWEETS = "sweets";
    public static final String TABLE_ALCOHOLIC_DRINKS = "alcoholic_drinks";
    public static final String TABLE_SOFT_DRINKS = "soft_drinks";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "product_name";
    public static final String COLUMN_PROTEIN = "product_protein";
    public static final String COLUMN_FAT = "product_fat";
    public static final String COLUMN_CARBOHYDRATE = "product_carbohydrate";
    public static final String COLUMN_CALORIES = "product_calories";
    public static final String COLUMN_IMAGE = "product_image";
    private Context myContext;

    public ExistingDbHelper(Context context) {
        super(context, DB_NAME, null, SCHEMA);
        this.myContext = context;
        DB_PATH = context.getFilesDir().getPath() + DB_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void create_db() {
        InputStream myInput = null;
        OutputStream myOutput = null;
        try {
            File file = new File(DB_PATH);
            if (!file.exists()) {
                this.getReadableDatabase();
                myInput = myContext.getAssets().open(DB_NAME);
                String outFileName = DB_PATH;

                myOutput = new FileOutputStream(outFileName);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }

                myOutput.flush();
                myOutput.close();
                myInput.close();
            }
        } catch (IOException ex) {
            Log.d("DatabaseHelper", ex.getMessage());
        }
    }

    public SQLiteDatabase open() throws SQLException {

        return SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
    }
}