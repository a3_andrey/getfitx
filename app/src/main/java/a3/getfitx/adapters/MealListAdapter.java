package a3.getfitx.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import a3.getfitx.R;


public class MealListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<String> id;
    private ArrayList<String> name;
    private ArrayList<String> protein;
    private ArrayList<String> fat;
    private ArrayList<String> carbohydrate;
    private ArrayList<String> calories;
    private ArrayList<String> weight;

    public MealListAdapter(Context mContext, ArrayList<String> id, ArrayList<String> mealName,
                           ArrayList<String> mealProtein, ArrayList<String> mealFat,
                           ArrayList<String> mealCarbohydrate, ArrayList<String> mealCalories,
                           ArrayList<String> mealWeight) {
        this.mContext = mContext;
        this.id = id;
        this.name = mealName;
        this.protein = mealProtein;
        this.fat = mealFat;
        this.carbohydrate = mealCarbohydrate;
        this.calories = mealCalories;
        this.weight = mealWeight;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int pos, View child, ViewGroup parent) {
        Holder mHolder;
        LayoutInflater layoutInflater;
        if (child == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.meal_list_row, null);

            mHolder = new Holder();
            mHolder.txtProductName = (TextView) child.findViewById(R.id.txtProductName);
            mHolder.txtProductProtein = (TextView) child.findViewById(R.id.txtProductProtein);
            mHolder.txtProductFat = (TextView) child.findViewById(R.id.txtProductFat);
            mHolder.txtProductCarbohydrate = (TextView) child.findViewById(R.id.txtProductCarbohydrate);
            mHolder.txtProductCalories = (TextView) child.findViewById(R.id.txtProductCalories);
            mHolder.txtProductWeight = (TextView) child.findViewById(R.id.txtProductWeight);
            child.setTag(mHolder);
        } else {
            mHolder = (Holder) child.getTag();
        }

        mHolder.txtProductName.setText(name.get(pos));
        mHolder.txtProductProtein.setText(protein.get(pos));
        mHolder.txtProductFat.setText(fat.get(pos));
        mHolder.txtProductCarbohydrate.setText(carbohydrate.get(pos));
        mHolder.txtProductCalories.setText(calories.get(pos));
        mHolder.txtProductWeight.setText(weight.get(pos));

        return child;
    }

    public class Holder {
        TextView txtProductName;
        TextView txtProductProtein;
        TextView txtProductFat;
        TextView txtProductCarbohydrate;
        TextView txtProductCalories;
        TextView txtProductWeight;
    }
}
